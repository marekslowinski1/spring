package com.marslow.season.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSoonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSoonApplication.class, args);
		System.out.println("Hello World!");
	}

}
