package com.marslow.season.spring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    /**
     *
     * @return warm greeting
     */
    @GetMapping("/hello")
    public String hello() {
        return "Hello World!";
    }
}
